# Aetherian Tap Ball
A game where you guide a ball through different courses.

![Image](ScreenShots/UnityGame.png)    ![Image](ScreenShots/UnityGame3.png)    ![Image](ScreenShots/UnityGame2.png)

## Requirements
    - Unity3D 2018
       -> Download latest version of Unity from Unity.com

## Installing
1. Download repo
2. Open Lobby from tower-defence/Assets/Levels/Lobby with Unity3D

### Build with
    Unity3D
    C#

### Authors
Joni Orrensalo
