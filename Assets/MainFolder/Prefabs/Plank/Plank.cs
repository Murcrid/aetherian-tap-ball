﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plank : MonoBehaviour
{
    [SerializeField]
    private GameObject partFront = null, partBack = null;
    public enum GapType { None, Small, Big }
    public enum WidthType { Normal, Small }
    public enum PlankType { Normal, NormalWSmallGap, NormalWBigGap,
                            Small, SmallWSmallGap, SmallWBigGap,
                            Moving, MovingWSmallGap, MovingWBigGap,
                            SmallMoving, SmallMovingWSmallGap, SmallMovingWBigGap,
                            Final, Null}
    public Transform mid, tail, head;
    [SerializeField]
    private GameObject[] finalPlankPieces = null;


    internal float TotalLength { get { return 10; } }
    internal float Width { get { return gameObject.transform.localScale.x; } }
    internal PlankType Type { get; private set; }

    private Vector3 originalPos;
    private bool isInitialized;
    private bool isAngled;

    private void Start()
    {
        if (!isInitialized)
        {
            Debug.Log("Non Initialized Plank... Destroying");
            Destroy(gameObject);
        }
    }

    internal void Initialize(PlankType type, bool isAngled)
    {
        isInitialized = true;
        Type = type;
        this.isAngled = isAngled;
        RandomizeStartingPosition();
        switch (type)
        {
            case PlankType.Normal:
                InitializeAsNormal();
                break;
            case PlankType.NormalWSmallGap:
                InitializeAsNormalWSmallGap();
                break;
            case PlankType.NormalWBigGap:
                InitializeAsNormalWBigGap();
                break;
            case PlankType.Small:
                InitializeAsSmall();
                break;
            case PlankType.SmallWSmallGap:
                InitializeAsSmallWSmallGap();
                break;
            case PlankType.SmallWBigGap:
                InitializeAsSmallWBigGap();
                break;
            case PlankType.Moving:
                InitializeAsMoving();
                break;
            case PlankType.MovingWSmallGap:
                InitializeAsMovingWSmallGap();
                break;
            case PlankType.MovingWBigGap:
                InitializeAsMovingWBigGap();
                break;
            case PlankType.SmallMoving:
                InitializeAsSmallMoving();
                break;
            case PlankType.SmallMovingWSmallGap:
                InitializeAsSmallMovingWSmallGap();
                break;
            case PlankType.SmallMovingWBigGap:
                InitializeAsSmallMovingWBigGap();
                break;
            case PlankType.Final:
                InitializeAsFinalPlank();
                break;
            case PlankType.Null:
                InitializeAsNull();
                break;
        }
    }

    private void InitializeAsSmallMovingWBigGap()
    {
        Debug.Log("Init as small moving W big gap");
        SetWidth(WidthType.Small);
        CreateGap(GapType.Big);
        StartCoroutine(SetMoving());
    }
    private void InitializeAsSmallMovingWSmallGap()
    {
        Debug.Log("Init as small moving W small gap");
        SetWidth(WidthType.Small);
        CreateGap(GapType.Small);
        StartCoroutine(SetMoving());
    }
    private void InitializeAsSmallMoving()
    {
        Debug.Log("Init as small moving");
        SetWidth(WidthType.Small);
        StartCoroutine(SetMoving());
    }
    private void InitializeAsMovingWBigGap()
    {
        Debug.Log("Init as moving W big gap");
        StartCoroutine(SetMoving());
        CreateGap(GapType.Big);
        SetWidth(WidthType.Normal);
    }
    private void InitializeAsMovingWSmallGap()
    {
        Debug.Log("Init as moving W small gap");
        StartCoroutine(SetMoving());
        CreateGap(GapType.Small);
        SetWidth(WidthType.Normal);
    }
    private void InitializeAsMoving()
    {
        Debug.Log("Init as moving");
        StartCoroutine(SetMoving());
        SetWidth(WidthType.Normal);
    }
    private void InitializeAsSmall()
    {
        Debug.Log("Initialized as Small");
        SetWidth(WidthType.Small);
    }
    private void InitializeAsSmallWSmallGap()
    {
        Debug.Log("Initialized as Small W Small Gap");
        SetWidth(WidthType.Small);
        CreateGap(GapType.Small);
    }
    private void InitializeAsSmallWBigGap()
    {
        Debug.Log("Initialized as Small W Big Gap");
        SetWidth(WidthType.Small);
        CreateGap(GapType.Big);
    }
    private void InitializeAsNull()
    {
        Debug.Log("Null plank initialized, destroying...");
        Destroy(gameObject);
    }
    private void InitializeAsFinalPlank()
    {
        Debug.Log("Intialized as Final plank");
        foreach (var piece in finalPlankPieces)
        {
            if (piece.activeSelf)
                piece.SetActive(false);
            else
                piece.SetActive(true);
        }
    }
    private void InitializeAsNormalWBigGap()
    {
        Debug.Log("Initialized as Normal W big gap");
        CreateGap(GapType.Big);
        SetWidth(WidthType.Normal);
    }
    private void InitializeAsNormalWSmallGap()
    {
        Debug.Log("Initialized as Normal W small gap");
        CreateGap(GapType.Small);
        SetWidth(WidthType.Normal);
    }
    private void InitializeAsNormal()
    {
        Debug.Log("Initialized as Normal");
        SetWidth(WidthType.Normal);
    }

    private void CreateGap(GapType type)
    {
        float randomPos = new System.Random().Next(0, 4);
        partBack.transform.localScale = new Vector3(partBack.transform.localScale.x, partBack.transform.localScale.y, partBack.transform.localScale.z + randomPos / 10);
        partBack.transform.localPosition = new Vector3(partBack.transform.localPosition.x, partBack.transform.localPosition.y, partBack.transform.localPosition.z - (randomPos * 0.5f));
        if (type == GapType.Small)
            randomPos += DataManager.instance.localData.SmallGap;
        else if(type == GapType.Big)
            randomPos += DataManager.instance.localData.BigGap;
        partFront.transform.localScale = new Vector3(partFront.transform.localScale.x, partFront.transform.localScale.y, partFront.transform.localScale.z - randomPos / 10);
        partFront.transform.localPosition = new Vector3(partFront.transform.localPosition.x, partFront.transform.localPosition.y, partFront.transform.localPosition.z - (randomPos * 0.5f));
    }
    private void SetWidth(WidthType type)
    {
        if (type == WidthType.Small)
            transform.localScale = new Vector3(DataManager.instance.localData.SmallPlankWidth, transform.localScale.y, transform.localScale.z);
        else if (type == WidthType.Normal)
            transform.localScale = new Vector3(DataManager.instance.localData.NormalPlankWidth, transform.localScale.y, transform.localScale.z);
    }
    private IEnumerator SetMoving()
    {
        bool dirBool = false;
        while (true)
        {
            if (dirBool)
            {
                transform.Translate(Vector3.right * Time.deltaTime * DataManager.instance.localData.PlankMovementSpeed * DataManager.instance.SelectedLevel.difficultyModifier);
                if ((transform.position.x - originalPos.x > 4.0f && isAngled) || (transform.position.z - originalPos.z < 0.0f && !isAngled))
                    dirBool = false;
            }
            else
            {
                transform.Translate(Vector3.left * Time.deltaTime * DataManager.instance.localData.PlankMovementSpeed * DataManager.instance.SelectedLevel.difficultyModifier);
                if ((transform.position.x - originalPos.x < 0.0f && isAngled) || (transform.position.z - originalPos.z > 4.0f && !isAngled))
                    dirBool = true;
            }
            yield return new WaitForSeconds(0);
        }
    }
    private void RandomizeStartingPosition()
    {
        int randomOffSet = new System.Random().Next(0, DataManager.instance.localData.PlankPositionMax);
        originalPos = transform.position;
        if (isAngled)
            transform.position = new Vector3(transform.position.x + randomOffSet, transform.position.y, transform.position.z);
        else
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + randomOffSet);
    }
}
