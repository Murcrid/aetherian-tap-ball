﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public enum Direction { Right, Forward}
    private Vector3 direction;
    private Rigidbody myRigidbody;

    private float jumpHeight = 1;

    internal float currentJumpDistance;
    internal float currentSpeed;

    internal bool jumping;

    private void Update()
    {
        if(!jumping)
            myRigidbody.velocity = new Vector3(0, myRigidbody.velocity.y, 0) + direction * currentSpeed;
    }
    internal void Initialize(float startingSpeed, float startingJumpDistance, Direction startingDirection)
    {
        myRigidbody = GetComponent<Rigidbody>();
        currentSpeed = startingSpeed;
        currentJumpDistance = startingJumpDistance;
        ChangeDir(startingDirection);
    }
    internal void ChangeDir(Direction dir)
    {
        direction = 
            dir == Direction.Forward ? Vector3.forward : Vector3.right;
    }
    internal void Jump()
    {
        float T = currentJumpDistance / currentSpeed;
        float G = (2 * jumpHeight) / (Mathf.Pow((T / 2), 2)); 
        Physics.gravity = new Vector3(0, -G, 0);
        float V = G * T / 2;
        myRigidbody.velocity += new Vector3(0, V, 0);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Goal")
            FindObjectOfType<GameManager>().LevelClear();
        else if (other.gameObject.tag == "Edge")
            FindObjectOfType<GameManager>().BallDropped();
        else
            direction =
                        direction == Vector3.right ? Vector3.left : Vector3.back;
    }
}
