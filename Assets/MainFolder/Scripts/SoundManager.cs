﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
            Destroy(this);
    }
    internal static SoundManager instance;

    public enum SFXNames { ButtonClick, Error, Jump, Lose_One, Lose_Two, Win_One, Win_Two, Purchase_Complete, Message, ChangeDirection }
    [SerializeField]
    private AudioClip[] SFXClips;
    public enum TrackNames { Menu_One, Game_Slow_One, Game_Slow_Two, Game_Fast_One }
    [SerializeField]
    private AudioClip[] TrackClips;

    [SerializeField]
    private AudioSource musicSource, sfxSource;

    internal static void GetValues()
    {
        instance.sfxSource.volume = DataManager.instance.localData.SFXVolume;
        instance.musicSource.volume = DataManager.instance.localData.MusicVolume;
        PlayMusic();
    }

    internal static void SwitchTrack(TrackNames track)
    {
        if (DataManager.instance.localData.MusicOn)
        {
            instance.musicSource.clip = instance.TrackClips[(int)track];
            PlayMusic();
        }
        else
            StopMusic();
    }

    internal static void PlaySFX(SFXNames sfx)
    {
        if (DataManager.instance.localData.SoundsOn)
        {
            instance.sfxSource.clip = instance.SFXClips[(int)sfx];
            instance.sfxSource.Play();
        }
    }

    internal static void AdjustMusicVolume(float volume)
    {
        if (volume < 0.1f || volume > 1.0f)
            return;
        else
        {
            DataManager.instance.localData.MusicVolume = volume;
            instance.musicSource.volume = DataManager.instance.localData.MusicVolume;
        }
    }

    internal static void AdjustSFXVolume(float volume)
    {
        if (volume < 0.1f || volume > 1.0f)
            return;
        else
        {
            DataManager.instance.localData.SFXVolume = volume;
            instance.sfxSource.volume = DataManager.instance.localData.SFXVolume;
        }
    }

    internal static void PlayMusic()
    {
        if (DataManager.instance.localData.MusicOn && !instance.musicSource.isPlaying)
            instance.musicSource.Play();
        else
            StopMusic();
    }

    internal static void StopMusic()
    {
        instance.musicSource.Stop();
    }
}
