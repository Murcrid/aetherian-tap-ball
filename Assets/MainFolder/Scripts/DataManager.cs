﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase.Auth;
using Firebase.Database;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DataManager : MonoBehaviour
{
    private FirebaseUser _FirebaseUser;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }
    internal static DataManager instance;

    internal LocalData localData;
    internal ServerData serverData;

    internal int levelIndex = 1;
    internal FirebaseUser FirebaseUser {
        get { return _FirebaseUser; }
        set {
            _FirebaseUser = value;
        }
    }

    internal int LevelCount { get { return localData.levels.Length; } }
    internal Level SelectedLevel { get { return localData.levels[levelIndex]; } }
    internal bool UserIsSignedIn { get { return FirebaseUser != null; } }

    internal void LoadLocalSaveState()
    {
        localData = new LocalData("");
        SoundManager.GetValues();
    }

    internal void LoadServerSaveState(bool changeUI)
    {
        serverData = new ServerData(FirebaseUser.UserId, changeUI);
    }

}