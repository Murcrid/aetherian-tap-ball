﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;     
    private static IExtensionProvider m_StoreExtensionProvider; 

    [SerializeField]
    internal static string no_ads = "no_ads";
    [SerializeField]
    internal static string lives_5 = "lives_5";
    [SerializeField]
    internal static string max_lives_3 = "max_lives_3";
    [SerializeField]
    internal static string double_lives_3 = "double_lives_3";

    void Start()
    {
        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
    }

    internal void InitializePurchasing()
    {
        if (IsInitialized())
            return;
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct(no_ads, ProductType.NonConsumable);
        builder.AddProduct(double_lives_3, ProductType.Consumable);
        builder.AddProduct(max_lives_3, ProductType.Consumable);
        builder.AddProduct(lives_5, ProductType.Consumable);
        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    internal Product GetInfoOfProduct(UIStoreMenu.PurchaseableItems item)
    {
        if (!IsInitialized())
            return null;
        switch (item)
        {
            case UIStoreMenu.PurchaseableItems.Lives_5:
                return m_StoreController.products.WithID(lives_5);
            case UIStoreMenu.PurchaseableItems.Max_Lives_3:
                return m_StoreController.products.WithID(max_lives_3); 
            case UIStoreMenu.PurchaseableItems.Double_Lives_3:
                return m_StoreController.products.WithID(double_lives_3); 
            case UIStoreMenu.PurchaseableItems.No_Ads:
                return m_StoreController.products.WithID(no_ads); 
        }
        return null;
    }

    internal void Purchase(UIStoreMenu.PurchaseableItems item)
    {
        switch (item)
        {
            case UIStoreMenu.PurchaseableItems.Lives_5:
                BuyProductID(lives_5);
                break;
            case UIStoreMenu.PurchaseableItems.Max_Lives_3:
                BuyProductID(max_lives_3);
                break;
            case UIStoreMenu.PurchaseableItems.Double_Lives_3:
                BuyProductID(double_lives_3);
                break;
            case UIStoreMenu.PurchaseableItems.No_Ads:
                if (!DataManager.instance.serverData.NoAds)
                    BuyProductID(no_ads);
                else
                    AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Error, "Already Purchased", "You already have purchased this item!"));
                break;
        }

    }

    internal void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
        }
        else
            Debug.Log("BuyProductID FAIL. Not initialized.");
    }

    internal void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, lives_5, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            DataManager.instance.serverData.RemainingLives += 5;
        }
        else if (String.Equals(args.purchasedProduct.definition.id, max_lives_3, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            DataManager.instance.serverData.MaxLives += 3;
        }
        else if (String.Equals(args.purchasedProduct.definition.id, double_lives_3, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            DataManager.instance.serverData.RemainingDoubleLives += 3;
        }
        else if (String.Equals(args.purchasedProduct.definition.id, no_ads, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            DataManager.instance.serverData.NoAds = true;

        }
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }
        SoundManager.PlaySFX(SoundManager.SFXNames.Lose_One);
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}

