﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Text scoreText = null;
    [SerializeField]
    private GameObject ballPrefab = null, plankPrefab = null;
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private GameObject voidTrigger;

    internal Plank currentPlank, nextPlank;
    internal Ball currentBall;
    internal int CurrentScore { get { return _CurrentScore; } set { _CurrentScore = value; UpdateScoreField(); } }

    private int _CurrentScore;
    private bool nextPlankIsAngled, ballGoingForward;
    private bool jumpOnCooldown, directionChangeOnCooldown;

    private void Start()
    {
        InitializeLevel();
        UpdateScoreField();
        CreatePlank();
        CreateNewBall();
        SoundManager.SwitchTrack(SoundManager.TrackNames.Game_Slow_One);
    }

    private void InitializeLevel()
    {
        DataManager.instance.SelectedLevel.ResetLevel();
    }

    private void Update()
    {
        if(currentBall != null)
            mainCamera.transform.position = currentBall.transform.position + DataManager.instance.localData.CameraOffset;
    }

    internal void GameOver()
    {
        if(voidTrigger.activeSelf)
            DataManager.instance.serverData.RemainingLives--;
        if (DataManager.instance.SelectedLevel.isInfinite)
            DataManager.instance.serverData.SetPersonalScore(CurrentScore);
        SoundManager.SwitchTrack(SoundManager.TrackNames.Menu_One);
        DataManager.instance.localData.GamesPlayed++;
        AppManager.instance.LoadLobbyScene();
    }
    private void CreatePlank()
    {
        if (currentPlank != null && nextPlank != null)
        {
            StartCoroutine(RemoveCurrentPlank(currentPlank.gameObject));
            CreateNextPlank();
        }
        else if(currentPlank != null && nextPlank == null)
        {
            CreateNextPlank();
        }
        else
        {
            currentPlank = Instantiate(plankPrefab).GetComponent<Plank>();
            currentPlank.Initialize(Plank.PlankType.Normal, nextPlankIsAngled);
            CreateNextPlank();
        }      
    }
    private void CreateNextPlank()
    {
        if (nextPlank != null)
        {
            currentPlank = nextPlank;
            currentPlank.StopAllCoroutines();
        }
        nextPlank = Instantiate(plankPrefab).GetComponent<Plank>();
        if (!nextPlankIsAngled)
        {
            nextPlank.transform.eulerAngles = currentPlank.transform.eulerAngles + new Vector3(0, 90, 0);
            nextPlank.transform.position = currentPlank.mid.position + new Vector3(currentPlank.Width / 2 + currentPlank.TotalLength / 2, 0, 0);
        }
        else
        {
            nextPlank.transform.eulerAngles = currentPlank.transform.eulerAngles + new Vector3(0, -90, 0);
            nextPlank.transform.position = currentPlank.mid.position + new Vector3(0, 0, currentPlank.Width / 2 + currentPlank.TotalLength / 2);
        }
        nextPlank.Initialize(DataManager.instance.SelectedLevel.NextPlank(), nextPlankIsAngled);
        nextPlankIsAngled = !nextPlankIsAngled;
    }
    private IEnumerator RemoveCurrentPlank(GameObject plankToRemove)
    {
        float delay = 2;
        while (delay > 0)
        {
            delay -= Time.deltaTime;
            yield return new WaitForSeconds(0);
        }
        plankToRemove.gameObject.AddComponent<Rigidbody>();
        Destroy(plankToRemove.gameObject, 2);
    }
    private void CreateNewBall()
    {
        currentBall = Instantiate(ballPrefab).GetComponent<Ball>();
        currentBall.transform.position = currentPlank.tail.position + new Vector3(0, 0.5f, 0);
        currentBall.Initialize(DataManager.instance.SelectedLevel.ballStartSpeed, DataManager.instance.localData.BallJumpDistance, nextPlankIsAngled ? Ball.Direction.Forward : Ball.Direction.Right);
    }
    private void UpdateScoreField()
    {
        if (!DataManager.instance.SelectedLevel.isInfinite)
            scoreText.text = CurrentScore + "/" + (DataManager.instance.SelectedLevel.TotalPlankCount + 1);
        else
            scoreText.text = CurrentScore.ToString();
    }
    internal void ChangeBallDirection()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ChangeDirection);
        if (!directionChangeOnCooldown)
        {
            StartCoroutine(BallDirectionChangeCooldown());
            CreatePlank();
            currentBall.ChangeDir(nextPlankIsAngled ? Ball.Direction.Forward : Ball.Direction.Right);
            CurrentScore++;
            currentBall.currentSpeed += DataManager.instance.SelectedLevel.difficultyModifier * 0.035f;
        }
    }

    private IEnumerator BallDirectionChangeCooldown()
    {
        float delay = DataManager.instance.SelectedLevel.ballStartSpeed / currentBall.currentSpeed;
        directionChangeOnCooldown = true;
        while (delay > 0)
        {
            delay -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        directionChangeOnCooldown = false;
    }

    internal void Jump()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.Jump);
        if (!jumpOnCooldown)
        {
            StartCoroutine(BallJumpCooldown());
            currentBall.Jump();
        }
    }

    private IEnumerator BallJumpCooldown()
    {
        float delay = currentBall.currentJumpDistance / currentBall.currentSpeed;
        jumpOnCooldown = true;
        currentBall.jumping = true;
        while (delay > 0)
        {
            delay -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        jumpOnCooldown = false;
        currentBall.jumping = false;
    }

    internal void UseDoubleLife()
    {
        DataManager.instance.serverData.RemainingDoubleLives--;
        CreateNewBall();
    }
    internal void BallDropped()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.Lose_One);
        if (DataManager.instance.serverData.RemainingDoubleLives > 0)
            AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Question, "You fell..", "You have double lives!\nUse one and continue?", UseDoubleLife, GameOver));
        else if(!DataManager.instance.SelectedLevel.isInfinite)
            AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Small, "You fell...", "Level failed... Try again!", GameOver));
        else
            AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Small, "You fell...", "Final score was " + CurrentScore + "!", GameOver));
    }
    internal void LevelClear()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.Win_One);
        voidTrigger.SetActive(false);
        if (DataManager.instance.levelIndex == DataManager.instance.serverData.HighestClearedLevelIndex)
        {
            DataManager.instance.serverData.HighestClearedLevelIndex++;
            AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Small, "Victory!", "Next level unlocked!", GameOver));
        }
        else
            AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Small, "Victory!", "Level cleared! ", GameOver));
    }
}
