﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class LocalData
{
    public Level[] levels;
    private LocalDataSaveState saveState;

    public bool MusicOn { get { return saveState.musicOn; } set { saveState.musicOn = value; SaveLocalSaveState(); } }
    public bool SoundsOn { get { return saveState.soundsOn; } set { saveState.soundsOn = value; SaveLocalSaveState(); } }
    public bool OneHandedModeOn { get { return saveState.oneHandedModeOn; } set { saveState.oneHandedModeOn = value; SaveLocalSaveState(); } }
    public bool LeftHandedModeOn { get { return saveState.leftHandedModeOn; } set { saveState.leftHandedModeOn = value; SaveLocalSaveState(); } }
    public float SFXVolume { get { return saveState.sfxVolume; } set { saveState.sfxVolume = value; SaveLocalSaveState(); } }
    public float MusicVolume { get { return saveState.musicVolume; } set { saveState.musicVolume = value; SaveLocalSaveState(); } }
    public int GamesPlayed { get; set; }
    public Vector3 CameraOffset { get { return new Vector3(-7.5f, 25, -7.5f); } }
    public float SmallGap { get { return 2.8f; } }
    public float BigGap { get { return 4.4f; } }
    public float SmallPlankWidth { get { return 0.6f; } }
    public float NormalPlankWidth { get { return 1; } }
    public float PlankMovementSpeed { get { return 1; } }
    public int PlankPositionMax { get { return 4; } }
    public float BallJumpDistance { get { return 2.5f; } }

    public LocalData(string uID)
    {
        levels = new Level[] {
            Resources.Load<Level>("Levels/Infinite"),   Resources.Load<Level>("Levels/Level_1"),    Resources.Load<Level>("Levels/Level_2"),
            Resources.Load<Level>("Levels/Level_3"),    Resources.Load<Level>("Levels/Level_4"),    Resources.Load<Level>("Levels/Level_5"),
            Resources.Load<Level>("Levels/Level_6"),    Resources.Load<Level>("Levels/Level_7"),    Resources.Load<Level>("Levels/Level_8"),
            Resources.Load<Level>("Levels/Level_9"),    Resources.Load<Level>("Levels/Level_10"),   Resources.Load<Level>("Levels/Level_11"),
            Resources.Load<Level>("Levels/Level_12"),    Resources.Load<Level>("Levels/Level_13"),   Resources.Load<Level>("Levels/Level_14"),
            Resources.Load<Level>("Levels/Level_15"),    Resources.Load<Level>("Levels/Level_16"),   Resources.Load<Level>("Levels/Level_17"),
            Resources.Load<Level>("Levels/Level_18"),   Resources.Load<Level>("Levels/Level_19"),   Resources.Load<Level>("Levels/Level_20"),
            Resources.Load<Level>("Levels/Level_21"),   Resources.Load<Level>("Levels/Level_22"),   Resources.Load<Level>("Levels/Level_23"),
            Resources.Load<Level>("Levels/Level_24"),   Resources.Load<Level>("Levels/Level_25"),   Resources.Load<Level>("Levels/Level_26"),
        };
        LoadLocalSaveState();
    }

    private void LoadLocalSaveState()
    {
        if (File.Exists(Path.Combine(Application.persistentDataPath, "LSS.sf")))
        {
            FileStream fs = new FileStream(Path.Combine(Application.persistentDataPath, "LSS.sf"), FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            saveState = (LocalDataSaveState)bf.Deserialize(fs);
            fs.Close();
        }
        else
            saveState = new LocalDataSaveState();
    }
    private void SaveLocalSaveState()
    {
        FileStream fs = new FileStream(Path.Combine(Application.persistentDataPath, "LSS.sf"), FileMode.OpenOrCreate);
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(fs, saveState);
        fs.Close();
    }
}
[Serializable]
public class LocalDataSaveState
{
    public bool musicOn;
    public bool soundsOn;
    public bool oneHandedModeOn;
    public bool leftHandedModeOn;

    public float sfxVolume = 0.5f;
    public float musicVolume = 0.5f;
}
[Serializable]
public class GlobalHighScores
{
    public GlobalHighScore score_1;
    public GlobalHighScore score_2;
    public GlobalHighScore score_3;
    public GlobalHighScore score_4;
    public GlobalHighScore score_5;
    public GlobalHighScore score_6;
    public GlobalHighScore score_7;
    public GlobalHighScore score_8;
    public GlobalHighScore score_9;
    public GlobalHighScore score_10;

    public int[] ValuesAsArray
    {
        get { return new int[] { score_1.score, score_2.score, score_3.score, score_4.score, score_5.score, score_6.score, score_7.score, score_8.score, score_9.score, score_10.score }; }
    }
    public string[] NamesAsArray
    {
        get { return new string[] { score_1.name, score_2.name, score_3.name, score_4.name, score_5.name, score_6.name, score_7.name, score_8.name, score_9.name, score_10.name }; }
    }
    public void SetName(int index, string name)
    {
        switch (index)
        {
            case 0:
                score_1.name = name;
                break;
            case 1:
                score_2.name = name;
                break;
            case 2:
                score_3.name = name;
                break;
            case 3:
                score_4.name = name;
                break;
            case 4:
                score_5.name = name;
                break;
            case 5:
                score_6.name = name;
                break;
            case 6:
                score_7.name = name;
                break;
            case 7:
                score_8.name = name;
                break;
            case 8:
                score_9.name = name;
                break;
            case 9:
                score_10.name = name;
                break;
            default:
                break;
        }
    }

    public int[] SetArray { set
        {
            if (value.Length != 10)
                return;
            score_1.score = value[0];
            score_2.score = value[1];
            score_3.score = value[2];
            score_4.score = value[3];
            score_5.score = value[4];
            score_6.score = value[5];
            score_7.score = value[6];
            score_8.score = value[7];
            score_9.score = value[8];
            score_10.score = value[9];
        }
    }
}
    [Serializable]
    public class GlobalHighScore
    {
        public int score;
        public string name;
    }

public class ServerData
{
    private ServerDataSaveState saveState = new ServerDataSaveState();
    private GlobalHighScores globalHighScores = new GlobalHighScores();
    private readonly string myUserID;

    public delegate void OnDatabaseUpdate(ChildChangedEventArgs e);
    public event OnDatabaseUpdate OnChildChangedInDatabase;

    internal bool skipNextLoad = false;

    public ServerData(string userID, bool changeUI)
    {
        myUserID = userID;
        LoadFromDataBase(myUserID, changeUI);
        LoadGlobalHighScores();
        FirebaseDatabase.DefaultInstance.GetReference("Users/" + userID).ChildChanged += MyData_ChildChanged;
        FirebaseDatabase.DefaultInstance.GetReference("HighScores").ChildChanged += HighScores_ChildChanged;
    }

    public bool HourlyAdWatched
    {
        get { return saveState.hourlyAdUsed; }
        set {
            saveState.hourlyAdUsed = value;
            skipNextLoad = true;
            SaveToDataBase(myUserID);
        }
    }

    public bool NoAds {
        get { return saveState.adsRemoved; }
        set
        {
            if (!saveState.adsRemoved)
            {
                saveState.adsRemoved = value;
                skipNextLoad = true;
                SaveToDataBase(myUserID);
            }
        }
    }

    internal string UserName
    {
        get { return saveState.userName; }
        set
        {
            if (saveState.userName == "")
            {
                saveState.userName = value;
                SaveToDataBase(myUserID);
            }
        }
    }

    internal int HighestClearedLevelIndex
    {
        get { return saveState.highestClearedLevelIndex; }
        set
        {
            saveState.highestClearedLevelIndex = value;
            skipNextLoad = true;
            SaveToDataBase(myUserID);
        }
    }
    internal int MaxLives
    {
        get { return saveState.maxLives; }
        set
        {
            saveState.maxLives = value;
            skipNextLoad = true;
            SaveToDataBase(myUserID);
        }
    }
    internal int RemainingLives
    {
        get { return saveState.remainingLives; }
        set
        {
            saveState.remainingLives = value;
            skipNextLoad = true;
            SaveToDataBase(myUserID);
        }
    }
    internal int RemainingDoubleLives
    {
        get { return saveState.remainingDoubleLives; }
        set
        {
            saveState.remainingDoubleLives = value;
            skipNextLoad = true;
            SaveToDataBase(myUserID);
        }
    }
    internal int[] PersonalScores
    {
        get { return new int[] { saveState.score_1, saveState.score_2, saveState.score_3, saveState.score_4, saveState.score_5 }; }
        private set
        {
            if (value.Length != 5)
                throw new UnassignedReferenceException();
            else
            {
                saveState.score_1 = value[0];
                saveState.score_2 = value[1];
                saveState.score_3 = value[2];
                saveState.score_4 = value[3];
                saveState.score_5 = value[4];
                SaveToDataBase(myUserID);
            }
        }
    }
    internal void SetPersonalScore(int value)
    {
        int[] temp = PersonalScores;
        for (int i = 0; i < PersonalScores.Length; i++)
        {
            if (PersonalScores[i] < value)
            {
                temp[i] = value;
                for (int x = i + 1; x < 4; x++)
                {
                    temp[x] = PersonalScores[i++];
                }
                PersonalScores = temp;
                break;
            }
        }
        int[] globalTemp = GlobalHighScores.ValuesAsArray;
        for (int i = 0; i < GlobalHighScores.ValuesAsArray.Length; i++)
        {
            if (GlobalHighScores.ValuesAsArray[i] < value)
            {
                globalTemp[i] = value;
                GlobalHighScores.SetName(i, saveState.userName);
                for (int x = i + 1; x < 9; x++)
                {
                    globalTemp[x] = GlobalHighScores.ValuesAsArray[i++];
                }
                GlobalHighScores.SetArray = globalTemp;
                break;
            }
        }
        UpdateGlobalHighScores();
    }
    internal GlobalHighScores GlobalHighScores { get { return globalHighScores; } }

    private void LoadGlobalHighScores()
    {
        FirebaseDatabase.DefaultInstance.GetReference("HighScores").GetValueAsync().ContinueWith((value) =>
        {
            if (value.IsFaulted)
            {
                Debug.Log("Highscores Query Faulted");
            }
            else if (value.IsCanceled)
            {
                Debug.Log("Highscores Query Canceled");
            }
            else if (value.Result.Value == null)
            {
                Debug.Log("Highscores Query was null");
                UpdateGlobalHighScores();
            }
            else
            {
                Debug.Log("Highscores Query was Succesful");
                globalHighScores = JsonUtility.FromJson<GlobalHighScores>(value.Result.GetRawJsonValue());
            }
        });
    }
    private void UpdateGlobalHighScores()
    {
        skipNextLoad = true;
        string json = JsonUtility.ToJson(globalHighScores);
        FirebaseDatabase.DefaultInstance.GetReference("HighScores").SetRawJsonValueAsync(json).ContinueWith((value) =>
        {
            if (value.IsFaulted)
            {
                Debug.Log("Highscores Update Faulted");
            }
            else if (value.IsCanceled)
            {
                Debug.Log("Highscores Update Canceled");
            }
            else
            {
                Debug.Log("Highscores Update Succesful! ");
            }
        });
    }
    private void LoadFromDataBase(string userID, bool switchUIElement)
    {
        FirebaseDatabase.DefaultInstance.GetReference("Users/" + userID).GetValueAsync().ContinueWith((value) =>
        {
            if (value.IsFaulted)
            {
                Debug.Log("Query Faulted");
            }
            else if (value.IsCanceled)
            {
                Debug.Log("Query Canceled");
            }
            else if (value.Result.Value == null)
            {
                Debug.Log("Query was null");
            }
            else if (value.IsCompleted)
            {
                Debug.Log("UserData Query was Succesful");
                saveState = JsonUtility.FromJson<ServerDataSaveState>(value.Result.GetRawJsonValue());
                if (switchUIElement)
                {
                    if (saveState.userName != "")
                        AppManager.instance.OpenMainMenu();
                    else
                        AppManager.instance.OpenWelcomeMenu();
                }
            }

        });
    }
    private void SaveToDataBase(string userID)
    {
        string json = JsonUtility.ToJson(saveState);
        FirebaseDatabase.DefaultInstance.GetReference("Users/" + userID).GetValueAsync().ContinueWith((getValue) =>
        {
            if (getValue.Result.Value != null)
            {
                Dictionary<string, object> dictionary = new Dictionary<string, object>();
                dictionary.Add("adsRemoved", saveState.adsRemoved);
                dictionary.Add("maxLives", saveState.maxLives);
                dictionary.Add("remainingLives", saveState.remainingLives);
                dictionary.Add("remainingDoubleLives", saveState.remainingDoubleLives);
                dictionary.Add("highestClearedLevelIndex", saveState.highestClearedLevelIndex);
                dictionary.Add("score_1", saveState.score_1);
                dictionary.Add("score_2", saveState.score_2);
                dictionary.Add("score_3", saveState.score_3);
                dictionary.Add("score_4", saveState.score_4);
                dictionary.Add("score_5", saveState.score_5);
                dictionary.Add("userName", saveState.userName);
                dictionary.Add("hourlyAdUsed", saveState.hourlyAdUsed);
                FirebaseDatabase.DefaultInstance.GetReference("Users/" + userID).UpdateChildrenAsync(dictionary).ContinueWith((value) =>
                {
                    if (value.IsFaulted)
                    {
                        Debug.Log("Update Faulted");
                    }
                    else if (value.IsCanceled)
                    {
                        Debug.Log("Update Canceled");
                    }
                    else
                    {
                        Debug.Log("Update Succesful! ");
                    }
                });
            }
            else
            {
                AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Error, "Network error", "Failed connecting up with the server. Please try again later"));
            }
        });
    }
    private void MyData_ChildChanged(object sender, ChildChangedEventArgs e)
    {
        if (e.Snapshot.Reference.ToString().EndsWith("remainingLives"))
            saveState.remainingLives = Int32.Parse(e.Snapshot.GetRawJsonValue());
        else if (e.Snapshot.Reference.ToString().EndsWith("hourlyAdUsed"))
            saveState.hourlyAdUsed = false;
        if (OnChildChangedInDatabase != null)
            OnChildChangedInDatabase(e);
    }
    private void HighScores_ChildChanged(object sender, ChildChangedEventArgs e)
    {
        if (e.Snapshot.Reference.ToString().EndsWith("1"))
            globalHighScores.score_1 = JsonUtility.FromJson<GlobalHighScore>(e.Snapshot.GetRawJsonValue());
        else if (e.Snapshot.Reference.ToString().EndsWith("2"))
            globalHighScores.score_2 = JsonUtility.FromJson<GlobalHighScore>(e.Snapshot.GetRawJsonValue());
        else if (e.Snapshot.Reference.ToString().EndsWith("3"))
            globalHighScores.score_3 = JsonUtility.FromJson<GlobalHighScore>(e.Snapshot.GetRawJsonValue());
        else if (e.Snapshot.Reference.ToString().EndsWith("4"))
            globalHighScores.score_4 = JsonUtility.FromJson<GlobalHighScore>(e.Snapshot.GetRawJsonValue());
        else if (e.Snapshot.Reference.ToString().EndsWith("5"))
            globalHighScores.score_5 = JsonUtility.FromJson<GlobalHighScore>(e.Snapshot.GetRawJsonValue());
        else if (e.Snapshot.Reference.ToString().EndsWith("6"))
            globalHighScores.score_6 = JsonUtility.FromJson<GlobalHighScore>(e.Snapshot.GetRawJsonValue());
        else if (e.Snapshot.Reference.ToString().EndsWith("7"))
            globalHighScores.score_7 = JsonUtility.FromJson<GlobalHighScore>(e.Snapshot.GetRawJsonValue());
        else if (e.Snapshot.Reference.ToString().EndsWith("8"))
            globalHighScores.score_8 = JsonUtility.FromJson<GlobalHighScore>(e.Snapshot.GetRawJsonValue());
        else if (e.Snapshot.Reference.ToString().EndsWith("9"))
            globalHighScores.score_9 = JsonUtility.FromJson<GlobalHighScore>(e.Snapshot.GetRawJsonValue());
        else if (e.Snapshot.Reference.ToString().EndsWith("10"))
            globalHighScores.score_10 = JsonUtility.FromJson<GlobalHighScore>(e.Snapshot.GetRawJsonValue());
        if (OnChildChangedInDatabase != null)
            OnChildChangedInDatabase(e);
    }
}
    public class ServerDataSaveState
    {
        public string userName = "";
        public int highestClearedLevelIndex = 1;
        public int maxLives = 5;
        public int remainingLives = 3;
        public int remainingDoubleLives = 1;
        public bool adsRemoved = false;
        public bool hourlyAdUsed = false;
        public int score_1, score_2, score_3, score_4, score_5;
    }

    [Serializable]
    public class PlankTypeData
    {
        public int originalCount;
        public int usedCount;
        public Plank.PlankType type;

        public PlankTypeData(Plank.PlankType type)
        {
            this.type = type;
        }
    }

