﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase.Auth;

public class AppManager : MonoBehaviour
{
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
            Destroy(this);
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;    
    }
    private void Start()
    {
        
        DataManager.instance.LoadLocalSaveState();
        LoadLobbyScene();
    }
    internal static AppManager instance; //Static instance of this   

    public delegate void AgreeButtonClick();

    private void ConfigureAspectRatio()
    {
        float targetaspect = 9.0f / 16.0f;
        float windowaspect = (float)Screen.width / (float)Screen.height;
        float scaleheight = windowaspect / targetaspect;
        Camera camera = FindObjectOfType<Camera>();
        if (scaleheight < 1.0f)
        {
            Rect rect = camera.rect;

            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;

            camera.rect = rect;
        }
        else if (scaleheight > 1.0f)
        {
            float scalewidth = 1.0f / scaleheight;

            Rect rect = camera.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;
     
            camera.rect = rect;
        }
    }

    [SerializeField]
    private GameObject  uiLogInMenuPrefab = null, uiMainMenuPrefab = null, uiLevelSelectMenuPrefab = null,                       //Prefabs for UI elements
                        uiStoreMenuPrefab = null, uiScoreMenuPrefab = null, uiOptionsMenuPrefab = null, uiGameMenuPrefab = null, //Prefabs for UI elements
                        uiPopUpInfoPrefab = null, uiLoadingPrefab = null, uiWelcomePrefab = null;                                      //Prefabs for UI elements
    private GameObject  currentlyActiveMenu, currentlyActivePopUpInfo;    //Currently instanstiated menu elements

    /// <summary>
    /// Opens Main Menu
    /// </summary>
    internal void OpenMainMenu()
    {
        if (currentlyActiveMenu != null)
            Destroy(currentlyActiveMenu);
        currentlyActiveMenu = Instantiate(uiMainMenuPrefab);
    }
    /// <summary>
    /// Opens Main Menu
    /// </summary>
    internal void OpenLoadingMenu()
    {
        if (currentlyActiveMenu != null)
            Destroy(currentlyActiveMenu);
        currentlyActiveMenu = Instantiate(uiLoadingPrefab);
    }
    /// <summary>
    /// Opens Main Menu
    /// </summary>
    internal void OpenWelcomeMenu()
    {
        if (currentlyActiveMenu != null)
            Destroy(currentlyActiveMenu);
        currentlyActiveMenu = Instantiate(uiWelcomePrefab);
    }
    /// <summary>
    /// Opens Log In Menu
    /// </summary>
    internal void OpenLogInMenu()
    {
        if (currentlyActiveMenu != null)
            Destroy(currentlyActiveMenu);
        currentlyActiveMenu = Instantiate(uiLogInMenuPrefab);
    }
    /// <summary>
    /// Opens Level Selections Menu
    /// </summary>
    internal void OpenLevelSelectionMenu()
    {
        if (currentlyActiveMenu != null)
            Destroy(currentlyActiveMenu);
        currentlyActiveMenu = Instantiate(uiLevelSelectMenuPrefab);
    }
    /// <summary>
    /// Opens Store Menu
    /// </summary>
    internal void OpenStoreMenu()
    {
        if (currentlyActiveMenu != null)
            Destroy(currentlyActiveMenu);
        currentlyActiveMenu = Instantiate(uiStoreMenuPrefab);
    }
    /// <summary>
    /// Opens Scores Menu
    /// </summary>
    internal void OpenScoresMenu()
    {
        if (currentlyActiveMenu != null)
            Destroy(currentlyActiveMenu);
        currentlyActiveMenu = Instantiate(uiScoreMenuPrefab);
    }
    /// <summary>
    /// Opens Options Menu
    /// </summary>
    internal void OpenOptionsMenu()
    {
        if (currentlyActiveMenu != null)
            Destroy(currentlyActiveMenu);
        currentlyActiveMenu = Instantiate(uiOptionsMenuPrefab);
    }
    /// <summary>
    /// Opens Game Menu
    /// </summary>
    internal void OpenGameMenu()
    {
        if (currentlyActiveMenu != null)
            Destroy(currentlyActiveMenu);
        currentlyActiveMenu = Instantiate(uiGameMenuPrefab);
    }
    /// <summary>
    /// Shows popupwindow with parameters from popUpData
    /// </summary>
    /// <param name="popUpData"></param>
    /// <returns></returns>
    internal bool ShowPopUpInfo(PopUpData popUpData)
    {
        if (currentlyActivePopUpInfo == null)
        {
            currentlyActivePopUpInfo = Instantiate(uiPopUpInfoPrefab);
            currentlyActivePopUpInfo.GetComponent<UiPopupInfo>().Initialize(popUpData);
            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// Logs user out of current account
    /// </summary>
    internal void LogOut()
    {
        FirebaseAuth.DefaultInstance.SignOut();
        OpenLogInMenu();
    }
    /// <summary>
    /// Logs user in with given credientals
    /// </summary>
    /// <param name="email"></param>
    /// <param name="password"></param>
    internal void LogIn(string email, string password)
    {
        FirebaseAuth.DefaultInstance.SignInWithEmailAndPasswordAsync(email, password).ContinueWith((result) =>
        {
            if (result.IsFaulted)
            {
                string errorMSG = result.Exception.ToString().Remove(0, result.Exception.ToString().LastIndexOf(":") + 1);
                ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Error, "Error!", errorMSG));
            }
            else if (result.IsCanceled)
                Debug.LogWarning("LogIn canceled");
            else
            {
                Debug.Log("LogIn Succes");
                DataManager.instance.FirebaseUser = result.Result;
                DataManager.instance.LoadServerSaveState(true);
                OpenLoadingMenu();
            }
        });
    }
    /// <summary>
    /// Creates account with given credientals
    /// </summary>
    /// <param name="email"></param>
    /// <param name="password"></param>
    internal void CreateAccount(string email, string password)
    {
        FirebaseAuth.DefaultInstance.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith((result) =>
        {
            if (result.IsFaulted)
            {
                string errorMSG = result.Exception.ToString().Remove(0, result.Exception.ToString().LastIndexOf(":") + 1);
                ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Error, "Error!", errorMSG));
            }
            else if (result.IsCanceled)
                Debug.LogWarning("Account Creation canceled");
            else
            {
                Debug.Log("Account Creation Succes");
                DataManager.instance.FirebaseUser = result.Result;
                instance.OpenWelcomeMenu();
            }
        });
    }
    /// <summary>
    /// Sends password reset email to given email
    /// </summary>
    /// <param name="text"></param>
    internal void SendResetPassword(string email)
    {
        FirebaseAuth.DefaultInstance.SendPasswordResetEmailAsync(email).ContinueWith((result) =>
        {
            if (result.IsFaulted)
                ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Error, "Error", result.Exception.Message));
            else if (result.IsCanceled)
                Debug.LogWarning("Password Reset canceled");
            else
                ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Small, "Succes!", "Password reset was sent. Please check your email for further instructions."));
        });
    }

    /// <summary>
    /// Call this to load Game scene and start game
    /// </summary>
    /// 
    internal void LoadGameScene()
    {
        SceneManager.LoadScene(2);
    }
    /// <summary>
    /// Call this to go to lobby scene
    /// </summary>
    internal void LoadLobbyScene()
    {
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// EventListener for scene changes
    /// </summary>
    /// <param name="scene"></param>
    /// <param name="mode"></param>
    private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == 1 && !DataManager.instance.UserIsSignedIn)
            OpenLogInMenu();
        else if (scene.buildIndex == 1 && DataManager.instance.UserIsSignedIn)
            OpenMainMenu();
        ConfigureAspectRatio();
    }
}
