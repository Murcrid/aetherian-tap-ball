﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameMenu : MonoBehaviour {

    [SerializeField]
    private RectTransform jumpButton = null, dirChangeButton = null;
    private GameManager gm;

    private void Start()
    {
        if (DataManager.instance.localData.OneHandedModeOn)
        {
            if (DataManager.instance.localData.LeftHandedModeOn)
            {
                jumpButton.anchorMax = new Vector2(0, 1);
                jumpButton.anchorMin = new Vector2(0, 1);
                jumpButton.pivot = new Vector2(0, 1);

                dirChangeButton.anchorMax = new Vector2(0, 0);
                dirChangeButton.anchorMin = new Vector2(0, 0);
                dirChangeButton.pivot = new Vector2(0, 0);
            }
            else
            {
                jumpButton.anchorMax = new Vector2(1, 1);
                jumpButton.anchorMin = new Vector2(1, 1);
                jumpButton.pivot = new Vector2(1, 1);
            }
        }
        jumpButton.anchoredPosition = new Vector2(0, 0);
        dirChangeButton.anchoredPosition = new Vector2(0, 0);
        gm = FindObjectOfType<GameManager>();
    }

    public void JumpButtonClick()
    {
        gm.Jump();
    }

    public void DirectionChangeButtonClick()
    {
        gm.ChangeBallDirection();
    }
}
