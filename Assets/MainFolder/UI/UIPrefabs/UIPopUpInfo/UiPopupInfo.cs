﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UiPopupInfo : MonoBehaviour
{
    [SerializeField]
    private Text bodyText = null, headerText = null, imageOneText = null, imageTwoText = null;
    [SerializeField]
    private GameObject yesButton = null, noButton = null, continueButton = null;
    [SerializeField]
    private Image imageOne = null, imageTwo = null;

    private PopUpData myData;
    private bool forceQuit;

    internal void Initialize(PopUpData data)
    {
        myData = data;
        transform.SetParent(GameObject.Find("MainCanvas").transform, false);
        UpdateFields();
    }
    private void Update()
    {
        transform.SetAsLastSibling();
        if (!forceQuit && myData != null && myData.duration >= 0)
            myData.duration -= myData.yesMethod == null ? Time.deltaTime : 0;
        else
            Destroy(gameObject);
    }

    private void UpdateFields()
    {
        switch (myData.type)
        {
            case PopUpData.InfoType.Error:
                SoundManager.PlaySFX(SoundManager.SFXNames.Error);
                SetTextColor(Color.red);
                ShowContinue("Close");
                break;
            case PopUpData.InfoType.Message_Small:
                SoundManager.PlaySFX(SoundManager.SFXNames.Message);
                SetTextColor(Color.black);
                ShowContinue("Close");
                break;
            case PopUpData.InfoType.Warning:
                SoundManager.PlaySFX(SoundManager.SFXNames.Message);
                SetTextColor(Color.yellow);
                ShowContinue("Close");
                break;
            case PopUpData.InfoType.Question:
                SoundManager.PlaySFX(SoundManager.SFXNames.Message);
                SetTextColor(Color.black);
                ShowYesNo();
                break;
            case PopUpData.InfoType.Message_Big:
                SoundManager.PlaySFX(SoundManager.SFXNames.Message);
                SetTextColor(Color.black);
                ShowContinue("Close");
                SetBodySizeAndData();
                break;
        }
        bodyText.text = myData.body;
        headerText.text = myData.header;
    }

    private void ShowYesNo()
    {
        continueButton.SetActive(false);
        yesButton.SetActive(true);
        noButton.SetActive(true);
    }
    private void ShowContinue(string text)
    {
        continueButton.SetActive(true);
        continueButton.GetComponentInChildren<Text>().text = text;
        yesButton.SetActive(false);
        noButton.SetActive(false);
    }
    private void SetTextColor(Color color)
    {
        bodyText.color = color;
        headerText.color = color;
    }

    private void SetBodySizeAndData()
    {
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(450, 840);
        headerText.transform.parent.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(380, 80);
        bodyText.transform.parent.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(350, 580);
        yesButton.GetComponent<RectTransform>().sizeDelta = new Vector2(180, 80);
        noButton.GetComponent<RectTransform>().sizeDelta = new Vector2(180, 80);
        continueButton.GetComponent<RectTransform>().sizeDelta = new Vector2(380, 80);

        if (myData.imageOne == null && myData.imageTwo == null)
        {
            imageOne.gameObject.SetActive(false);
            imageTwo.gameObject.SetActive(false);
            bodyText.GetComponent<RectTransform>().sizeDelta = new Vector2(300, 550);
        }
        else if (myData.imageOne != null && myData.imageTwo == null)
        {
            imageOne.gameObject.SetActive(true);
            imageOne.sprite = myData.imageOne;
            imageOneText.text = myData.imageOneText;
            imageTwo.gameObject.SetActive(false);
            bodyText.GetComponent<RectTransform>().sizeDelta = new Vector2(300, 460);
        }
        else
        {
            imageOne.gameObject.SetActive(true);
            imageOne.sprite = myData.imageOne;
            imageOneText.text = myData.imageOneText;
            imageTwo.gameObject.SetActive(true);
            imageTwo.sprite = myData.imageTwo;
            imageTwoText.text = myData.imageTwoText;
            bodyText.GetComponent<RectTransform>().sizeDelta = new Vector2(300, 370);
        }
    }

    public void YesButtonClick()
    {
        forceQuit = true;
        if (myData.yesMethod != null)
            myData.yesMethod();
    }

    public void NoButtonClick()
    {
        forceQuit = true;
        if (myData.noMethod != null)
            myData.noMethod();
    }
}

public class PopUpData
{
    public delegate void Method();
    public enum InfoType { Error, Message_Small, Warning, Question, Message_Big }
    public InfoType type;
    public string header;
    public string body;
    public float duration;
    public Sprite imageOne, imageTwo;
    public string imageOneText, imageTwoText;
    public Method yesMethod, noMethod;

    public PopUpData(InfoType type, string header, string body)
    {
        this.type = type;
        this.header = header;
        this.body = body;
        duration = 60.0f;
    }
    public PopUpData(InfoType type, string header, string body, Sprite image_one, string image_one_text)
    {
        this.type = type;
        this.header = header;
        this.body = body;
        duration = 60.0f;
        duration = 60.0f;
        imageOne = image_one;
        imageOneText = image_one_text;
    }
    public PopUpData(InfoType type, string header, string body, Sprite image_one, string image_one_text, Sprite image_two, string image_two_text)
    {
        this.type = type;
        this.header = header;
        this.body = body;
        duration = 60.0f;
        imageOne = image_one;
        imageOneText = image_one_text;
        imageTwo = image_two;
        imageTwoText = image_two_text;
    }
    public PopUpData(InfoType type, string header, string body, float duration)
    {
        this.type = type;
        this.header = header;
        this.body = body;
        this.duration = duration;
    }
    public PopUpData(InfoType type, string header, string body, Method yesMethod)
    {
        this.type = type;
        this.header = header;
        this.body = body;
        this.duration = 60.0f;
        this.yesMethod = yesMethod;
    }
    public PopUpData(InfoType type, string header, string body, Method yesMethod, Method noMethod)
    {
        this.type = type;
        this.header = header;
        this.body = body;
        this.duration = 60.0f;
        this.yesMethod = yesMethod;
        this.noMethod = noMethod;
    }
}
