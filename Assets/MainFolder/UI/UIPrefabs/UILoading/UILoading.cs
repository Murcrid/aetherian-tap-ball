﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILoading : UIMenu
{

    public GameObject loadingImageOne, loadingImageTwo;

    public override void BackButtonClick()
    {
        throw new System.NotImplementedException();
    }

    public override void InfoButtonClick()
    {
        throw new System.NotImplementedException();
    }

    // Update is called once per frame
    void Update () {
        loadingImageOne.transform.Rotate(new Vector3(0, 0, 1));
        loadingImageTwo.transform.Rotate(new Vector3(0, 0, 1));
    }
}
