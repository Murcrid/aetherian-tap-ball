﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Purchasing;

public class UIStoreMenu : UIMenu
{
    [SerializeField]
    private Text description = null, cost = null, itemName = null;
    [SerializeField]
    private Image image = null, lockImage = null;
    [SerializeField]
    private Sprite noAds = null, maxLives = null, fiveLives = null, doubleLives = null, locked = null, unlocked = null;
    public enum PurchaseableItems { Lives_5, Max_Lives_3, Double_Lives_3, No_Ads }

    private IAPManager IAPM;
    private PurchaseableItems item;
    private int index = 0;

    public void PuchaseSelectedItem()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        IAPM.Purchase((PurchaseableItems)index);
    }
    private void OnDestroy()
    {
        DataManager.instance.serverData.OnChildChangedInDatabase -= ServerData_OnChildChangedInDatabase;
    }

    protected override void Start()
    {
        IAPM = FindObjectOfType<IAPManager>();
        DataManager.instance.serverData.OnChildChangedInDatabase += ServerData_OnChildChangedInDatabase;
        UpdateVisuals();
        base.Start();
    }

    private void ServerData_OnChildChangedInDatabase(Firebase.Database.ChildChangedEventArgs e)
    {
        UpdateVisuals();
    }

    public override void InfoButtonClick()
    {
        AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Big, "Store Menu",
             "In store menu you can purchase: lives, double lives or max lives, or remove ads.\n\n" +
             "Lives:\n You need to have atleast one life to play the game.\n\n" +
             "Double Lives:\n When you fall in the game, double life gives you a chanse to continue from where you fell down.\n\n" +
             "Max Lives: \n Over time, you receive lives automatically. Max lives if the maximum amount that you can have at once.\n\n" +
             "Remove Ads:\n When purchased, the ads before starting a game are removed. You can still watch an ad to receive one life."));
    }
    public void NextItem()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        if (index < Enum.GetNames(typeof(PurchaseableItems)).Length - 1)
            index++;
        else
            index = 0;
        UpdateVisuals();
    }

    public void PreviousItem()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        if (index > 0)
            index--;
        else
            index = Enum.GetNames(typeof(PurchaseableItems)).Length - 1;
        UpdateVisuals();
    }

    private void UpdateVisuals()
    {
        Product product = IAPM.GetInfoOfProduct((PurchaseableItems)index);
        if (product != null)
        {
            switch ((PurchaseableItems)index)
            {
                case PurchaseableItems.Lives_5:
                    lockImage.sprite = unlocked;
                    image.sprite = fiveLives;
                    break;
                case PurchaseableItems.Max_Lives_3:
                    lockImage.sprite = unlocked;
                    image.sprite = maxLives;
                    break;
                case PurchaseableItems.Double_Lives_3:
                    lockImage.sprite = unlocked;
                    image.sprite = doubleLives;
                    break;
                case PurchaseableItems.No_Ads:
                    image.sprite = noAds;
                    if (DataManager.instance.serverData.NoAds)
                        lockImage.sprite = locked;
                    else
                        lockImage.sprite = unlocked;
                    break;
            }
            string fullItemName = product.metadata.localizedTitle;
            if (fullItemName.Contains("Five Lives") || fullItemName.Contains("lives_5"))
                itemName.text = "Five Lives";
            else if (fullItemName.Contains("Max lives 3") || fullItemName.Contains("max_lives_3"))
                itemName.text = "Three Max Lives";
            else if (fullItemName.Contains("Remove Ads") || fullItemName.Contains("no_ads"))
                itemName.text = "Remove Ads";
            else if (fullItemName.Contains("Three Double lives") || fullItemName.Contains("double_lives_3"))
                itemName.text = "Three Double Lives";
            else
                itemName.text = "-Unknown Item-";
            cost.text = product.metadata.localizedPriceString;
            description.text = product.metadata.localizedDescription;
        }
        else
        {
            itemName.text = "Error";
            cost.text = "Error";
            description.text = "Error";
        }
    }

    public override void BackButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.OpenMainMenu();
    }
}
