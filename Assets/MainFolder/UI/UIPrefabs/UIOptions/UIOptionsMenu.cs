﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOptionsMenu : UIMenu
{
    [SerializeField]
    private GameObject oneHandedOn, oneHandedOff, musicOn, musicOff, soundsOn, soundsOff, leftHand, leftHandedOn, leftHandedOff, musicVolume, sfxVolume;

    protected override void Start()
    {
        oneHandedOn.SetActive(DataManager.instance.localData.OneHandedModeOn);
        oneHandedOff.SetActive(!DataManager.instance.localData.OneHandedModeOn);
        leftHand.SetActive(DataManager.instance.localData.OneHandedModeOn);
        leftHandedOn.SetActive(DataManager.instance.localData.OneHandedModeOn && DataManager.instance.localData.LeftHandedModeOn);
        leftHandedOff.SetActive(DataManager.instance.localData.OneHandedModeOn && !DataManager.instance.localData.LeftHandedModeOn);

        musicOn.SetActive(DataManager.instance.localData.MusicOn);
        musicOff.SetActive(!DataManager.instance.localData.MusicOn);
        musicVolume.SetActive(DataManager.instance.localData.MusicOn);

        soundsOn.SetActive(DataManager.instance.localData.SoundsOn);
        soundsOff.SetActive(!DataManager.instance.localData.SoundsOn);
        sfxVolume.SetActive(DataManager.instance.localData.SoundsOn);

        sfxVolume.GetComponent<Slider>().value = DataManager.instance.localData.SFXVolume;
        musicVolume.GetComponent<Slider>().value = DataManager.instance.localData.MusicVolume;
        base.Start();
    }

    public override void InfoButtonClick()
    {
        AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Big, "Options Menu",
            "One Hand Mode\nMoves the jump button to same side with direction change.\n\n" +
            "Left Hand Mode\nMoves both buttons to the left side.\n\n" +
            "Music/SFX\nYou can change the volume of both, or disable them alltogether."));
    }

    public override void BackButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.OpenMainMenu();
    }
    public void OneHandedModeSwitch()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        DataManager.instance.localData.OneHandedModeOn = !DataManager.instance.localData.OneHandedModeOn;
        leftHand.SetActive(DataManager.instance.localData.OneHandedModeOn);
        leftHandedOn.SetActive(DataManager.instance.localData.OneHandedModeOn && DataManager.instance.localData.LeftHandedModeOn);
        leftHandedOff.SetActive(DataManager.instance.localData.OneHandedModeOn && !DataManager.instance.localData.LeftHandedModeOn);
    }
    public void MusicSwitch()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        DataManager.instance.localData.MusicOn = !DataManager.instance.localData.MusicOn;
        musicVolume.SetActive(DataManager.instance.localData.MusicOn);
        if (!DataManager.instance.localData.MusicOn)
            SoundManager.StopMusic();
        else
            SoundManager.PlayMusic();
    }
    public void SFXSwitch()
    {
        DataManager.instance.localData.SoundsOn = !DataManager.instance.localData.SoundsOn;
        sfxVolume.SetActive(DataManager.instance.localData.SoundsOn);
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
    }
    public void LeftHandedModeSwitch()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        DataManager.instance.localData.LeftHandedModeOn = !DataManager.instance.localData.LeftHandedModeOn;
    }
    public void ChangeMusicVolume()
    {
        SoundManager.AdjustMusicVolume(musicVolume.GetComponent<Slider>().value);
    }
    public void ChangeSFXVolume()
    {
        SoundManager.AdjustSFXVolume(sfxVolume.GetComponent<Slider>().value);
    }
}
