﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainMenu : UIMenu
{
    public void PlayButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.OpenLevelSelectionMenu();
    }
    public void StoreButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.OpenStoreMenu();
    }
    public void ScoresButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.OpenScoresMenu();
    }
    public void OptionsButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.OpenOptionsMenu();
    }
    public override void BackButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.LogOut();
    }

    public override void InfoButtonClick()
    {
        AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Big, "Main Menu",
                "Play\nPlay the game here!\n\n" +
                "Store\nPurchase additional lives and more!\n\n" +
                "Scores\nCheck your highscores and others are doing!\n\n" +
                "Options\nYou can change some settings here!"
                ));
    }
}
