﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScoresMenu : UIMenu
{
    [SerializeField]
    private GameObject[] scoresUnits; //Where scores are shown to user

    protected override void Start()
    {
        DataManager.instance.serverData.OnChildChangedInDatabase += ServerData_OnChildChangedInDatabase;
        ShowMyScoresButtonClick();
        base.Start();
    }

    private void OnDestroy()
    {
        DataManager.instance.serverData.OnChildChangedInDatabase -= ServerData_OnChildChangedInDatabase;
    }

    private void ServerData_OnChildChangedInDatabase(Firebase.Database.ChildChangedEventArgs e)
    {
        ShowGlobalScoresButtonClick();
    }
    public override void InfoButtonClick()
    {
        AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Big, "Scores Menu",
            "Scores menu shows your five best scores in \"Infinity\" mode.\n\n" +
            "You can also see top ten scores in \"Infinity\" mode from all players."));
    }
    /// <summary>
    /// Shows personal scores in scoresUnits
    /// </summary>
    public void ShowMyScoresButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        int[] myHighScores = DataManager.instance.serverData.PersonalScores;
        for (int i = 0; i < 10; i++)
        {
            if (i < 5)
            {
                scoresUnits[i].transform.GetChild(0).GetComponent<Text>().text = myHighScores[i].ToString();
                scoresUnits[i].transform.GetChild(1).GetComponent<Text>().text = "";
            }
            else
                scoresUnits[i].SetActive(false);
        }
    }
    /// <summary>
    /// Shows global scores in scoresUnits
    /// </summary>
    public void ShowGlobalScoresButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        GlobalHighScores globalHighScores = DataManager.instance.serverData.GlobalHighScores;
        for (int i = 0; i < 10; i++)
        {
            scoresUnits[i].SetActive(true);
            scoresUnits[i].transform.GetChild(0).GetComponent<Text>().text = globalHighScores.ValuesAsArray[i].ToString();
            scoresUnits[i].transform.GetChild(1).GetComponent<Text>().text = globalHighScores.NamesAsArray[i];
        }
    }
    /// <summary>
    /// Returns you to main menu
    /// </summary>
    public override void BackButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.OpenMainMenu();
    }
}
