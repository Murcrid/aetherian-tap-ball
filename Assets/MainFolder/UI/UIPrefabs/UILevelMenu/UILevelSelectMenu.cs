﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILevelSelectMenu : UIMenu
{
    [SerializeField]
    private GameObject infinityModePanel = null, levelModePanel = null;
    [SerializeField]
    private Text levelName = null, lives_maxLives = null, doubleLives = null;
    [SerializeField]
    private Image livesImage = null, doubleLivesImage = null;

    private bool infinityModeIsOn;

    protected override void Start()
    {
        if (DataManager.instance.levelIndex == 0)
            infinityModeIsOn = true;
        UpdateMenu();
        DataManager.instance.serverData.OnChildChangedInDatabase += ServerData_OnChildChangedInDatabase;
        base.Start();
    }

    private void OnDestroy()
    {
        DataManager.instance.serverData.OnChildChangedInDatabase -= ServerData_OnChildChangedInDatabase;
    }

    private void ServerData_OnChildChangedInDatabase(Firebase.Database.ChildChangedEventArgs e)
    {
        UpdateMenu();
    }

    public override void InfoButtonClick()
    {
        AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Big, "Level Selection",
                        "This is level selection menu.\nHere you can choose the level you want to play, " +
                        "or you can choose to play infinity mode where the planks keep coming until you fall.", 
                        livesImage.sprite, "= your remaining lives out of max lives", doubleLivesImage.sprite, "= your remaining double lives"
                        ));
    }


    public void OpenInfinityModeButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        infinityModeIsOn = true;
        DataManager.instance.levelIndex = 0;
        infinityModePanel.SetActive(infinityModeIsOn);
        levelModePanel.SetActive(!infinityModeIsOn);
    }
    public void OpenLevelModeButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        infinityModeIsOn = false;
        DataManager.instance.levelIndex = 1;
        infinityModePanel.SetActive(infinityModeIsOn);
        levelModePanel.SetActive(!infinityModeIsOn);
        UpdateMenu();
    }
    public void SelectNextLevelButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        if (DataManager.instance.levelIndex < DataManager.instance.LevelCount - 1)
            DataManager.instance.levelIndex++;
        else
            DataManager.instance.levelIndex = 1;
        UpdateMenu();
    }
    public void SelectPreviousLevelButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        if (DataManager.instance.levelIndex > 1)
            DataManager.instance.levelIndex--;
        else
            DataManager.instance.levelIndex = DataManager.instance.LevelCount - 1;
        UpdateMenu();
    }
    public void PlaySelectedLevelButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        if (DataManager.instance.serverData.HighestClearedLevelIndex < DataManager.instance.levelIndex)
            AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Small, "Level error", "Level not yet unlocked."));
        else if (DataManager.instance.serverData.RemainingLives <= 0 && !DataManager.instance.serverData.HourlyAdWatched)
            AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Question, "Watch add?", "You are out of lives. Watch ad to earn one?", AdsManager.PlayRewardAd));
        else if (DataManager.instance.serverData.RemainingLives <= 0)
            AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Small, "No Lives", "No remaining lives! Please purchase more or wait!"));
        else if(DataManager.instance.localData.GamesPlayed % 3 == 0 && DataManager.instance.localData.GamesPlayed > 0)
        {
            AdsManager.PlayAdd();
            AppManager.instance.LoadGameScene();
        }
        else
            AppManager.instance.LoadGameScene();
    }
    public override void BackButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.OpenMainMenu();
    }

    private void UpdateMenu()
    {
        if (!infinityModeIsOn)
        {
            levelName.text = DataManager.instance.SelectedLevel.name;
            if (DataManager.instance.serverData.HighestClearedLevelIndex < DataManager.instance.levelIndex)
                levelName.color = Color.red;
            else
                levelName.color = Color.white;
        }
        lives_maxLives.text = DataManager.instance.serverData.RemainingLives + "/" + DataManager.instance.serverData.MaxLives;
        doubleLives.text = DataManager.instance.serverData.RemainingDoubleLives.ToString();
    }
}
