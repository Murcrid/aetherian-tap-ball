﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Auth;

public class UILogInMenu : UIMenu
{
    [SerializeField]
    private GameObject logInMenu = null, accountCreationMenu = null, passwordResetMenu = null;
    [SerializeField]
    private InputField logInEmailField = null, logInPasswordField = null, 
                       accountCreationEmailField = null, accountCreationPasswordField = null,
                       resetPasswordEmailField = null;

    public override void InfoButtonClick()
    {
        AppManager.instance.ShowPopUpInfo(new PopUpData(PopUpData.InfoType.Message_Big, "Log In Menu", 
            "Welcome to Aetherian Tap Ball!!.\n\n" +
            "Please log in using your account information.\n\n" +
            "If you dont have an account yet, please create one.\n\n" +
            "If you have forgotten your password you can choose \"Restore\" to send an email for further instructions."));
    }

    public void OpenLogInButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        logInMenu.SetActive(true);
        accountCreationMenu.SetActive(false);
        passwordResetMenu.SetActive(false);
    }
    public void OpenAccountCreationButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        logInMenu.SetActive(false);
        accountCreationMenu.SetActive(true);
        passwordResetMenu.SetActive(false);
    }
    public void OpenPasswordResetButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        logInMenu.SetActive(false);
        accountCreationMenu.SetActive(false);
        passwordResetMenu.SetActive(true);
    }
    public void CreateAccountButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.CreateAccount(accountCreationEmailField.text, accountCreationPasswordField.text);
    }
    public void LogInButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.LogIn(logInEmailField.text, logInPasswordField.text);
    }
    public void SendResetEmailButtonClick()
    {
        SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
        AppManager.instance.SendResetPassword(resetPasswordEmailField.text);
    }
    public override void BackButtonClick()
    {
        Application.Quit();
    }
}
