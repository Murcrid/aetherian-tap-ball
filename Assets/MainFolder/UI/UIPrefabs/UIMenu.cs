﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIMenu : MonoBehaviour {

    protected virtual void Start()
    {
        transform.SetParent(GameObject.Find("MainCanvas").transform, false);
    }

    public abstract void InfoButtonClick();
    public abstract void BackButtonClick();
}
