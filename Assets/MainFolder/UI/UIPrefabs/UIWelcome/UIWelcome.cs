﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWelcome : UIMenu {

    [SerializeField]
    private InputField userNameInput;
    [SerializeField]
    private GameObject info, eula;

    public override void BackButtonClick()
    {
        throw new System.NotImplementedException();
    }
    public override void InfoButtonClick()
    {
        throw new System.NotImplementedException();
    }

    public void AcceptEULA()
    {
        info.SetActive(true);
        eula.SetActive(false);
        DataManager.instance.LoadServerSaveState(false);
    }

    public void SubmitButtonClick()
    {
        if (userNameInput.text != "")
        {
            SoundManager.PlaySFX(SoundManager.SFXNames.ButtonClick);
            DataManager.instance.serverData.UserName = userNameInput.text;
            AppManager.instance.OpenMainMenu();

        }
        else
            SoundManager.PlaySFX(SoundManager.SFXNames.Error);
    }
}
